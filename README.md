# RxAndroid #
1. Setup the gradle *compile 'io.reactivex:rxandroid:0.25.0'*
2. You can think of an observable as an object that emits data and an observer as an object that consumes that data.
      **observers** are instances of the Observer interface, and **observables** are instances of the Observable class.
3. The Observable class has many static methods, called operators, to create Observable objects.
4. The Observer interface has intuitively named methods to handle the different types of notifications it can receive from the observable.
5. Assign an observer to an observable, you should use the subscribe method, which returns a Subscription object.

* http://code.tutsplus.com/tutorials/getting-started-with-reactivex-on-android--cms-24387*

* http://blog.danlew.net/2014/09/15/grokking-rxjava-part-1/*

# ButterKnife #
1. Add dependency *compile 'com.jakewharton:butterknife:6.1.0'*
2. Use annotations. @InjectView(R.id.sample_textview) TextView sample_textview;
3. In the onCreate() method of the activity, before using any the views, call inject on the Butterknife object.
          *ButterKnife.inject(this);*
4. If you are using fragments, you have to specify the source of the views in the onCreateView() method as shown below.
         *View view = inflater.inflate(R.layout.sample_fragment, null);*
 *       ButterKnife.inject(this, view);*

 * http://code.tutsplus.com/tutorials/quick-tip-using-butter-knife-to-inject-views-on-android--cms-23542*