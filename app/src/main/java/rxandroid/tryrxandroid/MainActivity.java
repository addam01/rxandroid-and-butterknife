package rxandroid.tryrxandroid;

/**RxAndroid
 * 1. Setup the gradle compile 'io.reactivex:rxandroid:0.25.0'
 * 2. You can think of an observable as an object that emits data and an observer as an object that consumes that data.
 *      *observers are instances of the Observer interface, and observables are instances of the Observable class.
 * 3. The Observable class has many static methods, called operators, to create Observable objects.
 * 4. The Observer interface has intuitively named methods to handle the different types of notifications it can receive from the observable.
 * 5. Assign an observer to an observable, you should use the subscribe method, which returns a Subscription object.
 *
 * http://code.tutsplus.com/tutorials/getting-started-with-reactivex-on-android--cms-24387
 *
 * http://blog.danlew.net/2014/09/15/grokking-rxjava-part-1/
 * **/

/**ButterKnife
 * 1. Add dependency compile 'com.jakewharton:butterknife:6.1.0'
 * 2. Use annotations. @InjectView(R.id.sample_textview) TextView sample_textview;
 * 3. In the onCreate() method of the activity, before using any the views, call inject on the Butterknife object.
 *          ButterKnife.inject(this);
 * 4. If you are using fragments, you have to specify the source of the views in the onCreateView() method as shown below.
 *     View view = inflater.inflate(R.layout.sample_fragment, null);
 *       ButterKnife.inject(this, view);
 *
 * http://code.tutsplus.com/tutorials/quick-tip-using-butter-knife-to-inject-views-on-android--cms-23542
 * **/


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Observer;

import butterknife.ButterKnife;
import butterknife.InjectView;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.view.OnClickEvent;
import rx.android.view.ViewObservable;
import rx.functions.Action1;

public class MainActivity extends AppCompatActivity {

    @InjectView(R.id.button1)
    Button button1;

    @InjectView(R.id.text)
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        rx.Observable<OnClickEvent> clicksObservable
                = ViewObservable.clicks(button1); // Create a ViewObservable for the Button
        clicksObservable
                .skip(5)
                .subscribe(new Action1<OnClickEvent>() {
                    @Override
                    public void call(OnClickEvent onClickEvent) {
                        Toast.makeText(getApplicationContext(),"U have clicked",Toast.LENGTH_LONG).show();
                    }
                });

        /**Observable that hold some way to showing data and the data**/
        Observable<String> myHelloWorld = Observable.just("Hello World HERPDERP");

        /**Observer that handles the observable data of what to do with it**/
        rx.Observer<String> myObserver = new rx.Observer<String>(){

            @Override
            public void onCompleted() {
                //Called when there is no data
            }

            @Override
            public void onError(Throwable e) {
                //Called when it has error
            }

            @Override
            public void onNext(String s) {
                //Called when each time the observer emits data
                Log.d("MY OBSERVER", s);
            }
        };

        /**A subscription to bind the observable to the observer, Like putting a window(Observable) for ppl(Observer) to see**/
        Subscription mySub = myHelloWorld.subscribe(myObserver);
        /**Similar to observer also, but with action and listeners**/
            Action1<String> onNextAction = new Action1<String>() {
            @Override
            public void call(String s) {
                text.setText(s);
            }
        };
        myHelloWorld.subscribe(onNextAction);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
